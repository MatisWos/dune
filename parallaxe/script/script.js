/* eslint-disable no-var */
/* eslint-disable no-unused-vars */
/* eslint-disable require-jsdoc */
/* eslint-disable max-len */
// Obligatoire pour Codesandbox (pb de dépendances...)
// Le fichier script.js est donc ici inutile...
// Le script est placé à la fin du body car l'attribut defer ne fonctionne que pour des scripts externes
// CE N'EST PAS UNE BONNE PRATIQUE !!!

/* Burger Menu*/
$('.toggle-icon').click(function() {
  $('#nav-container').toggleClass('pushed');
});

// Variable globale contenant l'état du lecteur
let etatLecteur;

function lecteurPret(event) {
  // event.target = lecteur
  event.target.setVolume(50);
}

function changementLecteur(event) {
  // event.data = état du lecteur
  etatLecteur = event.data;
}

let lecteur;

function onYouTubeIframeAPIReady() {
  lecteur = new YT.Player('video', {
    height: '390',
    width: '640',
    videoId: 'n9xhJrPXop4',
    playerVars: {
      color: 'white',
      enablejsapi: 1,
      modestbranding: 1,
      rel: 0,
    },
    events: {
      onReady: lecteurPret,
      onStateChange: changementLecteur,
    },
  });
}

// Hauteur de la vidéo
const hauteurVideo = $('#video').height();
// Position Y de la vidéo
const posYVideo = $('#video').offset().top;
// Valeur declenchant la modification de l'affichage (choix "esthétique")
const seuil = posYVideo + 0.75 * hauteurVideo;

// Gestion du défilement
$(window).scroll(function() {
  // Récupération de la valeur du défilement vertical
  const scroll = $(window).scrollTop();

  // Classe permettant l'exécution du CSS
  $('#video').toggleClass(
      'scroll',
      etatLecteur === YT.PlayerState.PLAYING && scroll > seuil,
  );
});

/* Carrousel 1*/
const pre = document.querySelector('main>section:nth-of-type(3)>div>p:nth-of-type(1)');
const suiv = document.querySelector('main>section:nth-of-type(3)>div>p:nth-of-type(2)');
const diapos = document.querySelectorAll('main>section:nth-of-type(3)>div>div>img');
const textes = document.querySelectorAll('main>section:nth-of-type(3)>div>div>p');

let index = 0;

// Click sur le bouton suiv
suiv.addEventListener('click', () => {
  // Ce que l'on fait
  index += 1;
  if (index === diapos.length) {
    index = 0;
  }

  afficheDiapo(index);

  if (index === textes.length) {
    index = 0;
  }

  afficheTexte(index);
});

// Click sur le bouton pre
pre.addEventListener('click', () => {
  // Ce que l'on fait
  index -= 1;
  if (index === -1) {
    index = diapos.length - 1;
  }

  afficheDiapo(index);

  if (index === -1) {
    index = textes.length - 1;
  }
  afficheTexte(index);
});

// Fonction afficher une diapo
function afficheDiapo(indexDiapo) {
  // Ce que fait la fonction
  // Effacer toutes les images
  diapos.forEach((diapo) => {
    diapo.classList.remove('display');
  });
  diapos[indexDiapo].classList.add('display');
}

// Fonction afficher un texte
function afficheTexte(indexTexte) {
  // Ce que fait la fonction
  // Effacer toutes les images
  textes.forEach((texte) => {
    texte.classList.remove('display');
  });
  textes[indexTexte].classList.add('display');
}
/* Carrousel 2*/
const pre2 = document.querySelector('main>section:nth-of-type(4)>div>p:nth-of-type(1)');
const suiv2 = document.querySelector('main>section:nth-of-type(4)>div>p:nth-of-type(2)');
const diapos2 = document.querySelectorAll('main>section:nth-of-type(4)>div>div>img');
let index2 = 0;

// Click sur le bouton suiv
suiv2.addEventListener('click', () => {
  // Ce que l'on fait
  index2 += 1;
  if (index2 === diapos2.length) {
    index2 = 0;
  }

  afficheDiapo2(index2);
});

// Click sur le bouton pre
pre2.addEventListener('click', () => {
  // Ce que l'on fait
  index2 -= 1;
  if (index2 === -1) {
    index2 = diapos2.length - 1;
  }

  afficheDiapo2(index2);
});

// Fonction afficher une diapo
function afficheDiapo2(indexDiapo) {
  // Ce que fait la fonction
  // Effacer toutes les images
  diapos2.forEach((diapo) => {
    diapo.classList.remove('display');
  });
  diapos2[indexDiapo].classList.add('display');
}

// Fonction de défilement automatique
function defiler() {
  // Augmenter l'indice de la diapo
  index2 += 1;

  // Vérifier si l'on a atteint la dernière diapo
  if (index2 === diapos2.length) {
    // Redémarrer à la première diapo
    index2 = 0;
  }

  // Afficher la nouvelle diapo
  afficheDiapo2(index2);
}

// Démarrage du défilement automatique
setInterval(defiler, 3000);


// ------------------------------------MAP ---------------------------------

// Création de la carte, vide à ce stade
var carte = L.map('carte').setView([47.31253336328517, 5.091069784448199], 13);

// Ajout des tuiles (ici OpenStreetMap)
// https://wiki.openstreetmap.org/wiki/Tiles#Servers
L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(carte);
// Ajout de l'échelle
L.control.scale().addTo(carte);

var marker = L.marker([47.31253336328517, 5.091069784448199]).addTo(carte);
var popup = L.popup()
    .setLatLng([47.312533363285, 5.091069784448199])
    .setContent('oh punaise')
    .openOn(carte);


function onEachFeature(feature, layer) {
  // does this feature have a property named popupContent?
  if (feature.properties && feature.properties.NOM_ETABLISSEMENT) {
    layer.bindPopup('<h2>' + feature.properties.NOM_ETABLISSEMENT + '</h2>' +
                     '<p>Nombre de salle : ' + feature.properties.ECRANS + '</p>');
  }
}

L.geoJSON(cinema, {
  onEachFeature: onEachFeature,
}).addTo(carte);

// ------------------------------------LOCALISATION---------------------------------
const texte = document.querySelector('main>section:nth-of-type(5)>div>p');
// Appelée si récupération des coordonnées réussie
function positionSucces(position) {
  // Injection du résultat dans du texte
  const lat = Math.round(1000 * position.coords.latitude) / 1000;
  const long = Math.round(1000 * position.coords.longitude) / 1000;
  $(texte).text(`Latitude: ${lat}°, Longitude: ${long}°`);
}

// Appelée si échec de récuparation des coordonnées
function positionErreur(erreurPosition) {
  // Cas d'usage du switch !
  let natureErreur;
  switch (erreurPosition.code) {
    case erreurPosition.TIMEOUT:
      // Attention, durée par défaut de récupération des coordonnées infini
      natureErreur = "La géolocalisation prends trop de temps...";
      break;
    case erreurPosition.PERMISSION_DENIED:
      natureErreur = "Vous n'avez pas autorisé la géolocalisation.";
      break;
    case erreurPosition.POSITION_UNAVAILABLE:
      natureErreur = "Votre position n'a pu être déterminée.";
      break;
    default:
      natureErreur = "Une erreur inattendue s'est produite.";
  }
  // Injection du texte
  $(texte).text(natureErreur);
}

// Récupération des coordonnées au clic sur le bouton

$("button").click(function () {
  // Support de la géolocalisation
  if ("geolocation" in navigator) {
    // Support = exécution du callback selon le résultat
    navigator.geolocation.getCurrentPosition(positionSucces, positionErreur, {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 30000
    });
  } else {
    // Non support = injection de texte
    $(texte).text("La géolocalisation n'est pas supportée par votre navigateur");
  }
});